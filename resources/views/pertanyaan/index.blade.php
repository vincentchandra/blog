@extends('adminlte.master')
@section('content')
<div class="card">

    <div class="card-header">
        <h3 class="card-title">Daftar Pertanyaan</h3>
    </div>
    <div class="card-body">
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <a href="/pertanyaan/create" class="btn btn-primary mb-3">Buat Pertanyaan</a>
        <table class="table table-bordered">
            <thead>                  
            <tr>
                <th style="width: 10px">#</th>
                <th>Judul</th>
                <th>Isi</th>
                <th style="width: 40px">Label</th>
            </tr>
            </thead>
            <tbody>
            
            @forelse ($questions as $key => $question)
            <tr>
                <td>{{ $key + 1 }}</td>
                <td>{{ $question->judul }}</td>
                <td>{{ $question->isi }}</td>
                <td style="display: flex; ">
                    <a href="/pertanyaan/{{ $question->id }}" class="btn btn-info btn-sm">show</a>
                    <a style="margin-left: 5px;" href="/pertanyaan/{{ $question->id }}/edit" class="btn btn-warning btn-sm">edit</a>
                    <form style="margin-left: 5px;" action="/pertanyaan/{{$question->id}}" method="post">
                        @csrf
                        @method('delete')
                        <input type="submit" value="delete" class="btn btn-danger btn-sm" >
                    </form>
                </td>
            </tr>
            @empty
            <tr>
                <td colspan="4" align="center">Tidak Ada Pertanyaan</td>
            </tr>
            @endforelse
           
            
            </tbody>
        </table>
    </div>
</div>
@endsection