<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class PertanyaanController extends Controller
{
    //
    public function create(){
        return view('pertanyaan.create');
    }
    public function store(Request $request){
        // dd($request->all());
        $request->validate([
            'judul'=>'required|unique:pertanyaan|max:255',
            'isi'=>'required'
        ]);

        $query = DB::table('pertanyaan')->insert([
            'judul' =>  $request['judul'],
            'isi' =>  $request['isi']

        ]);
        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Disimpan');
    }
    public function index(){
        $questions = DB::table('pertanyaan')->get();
        return view('pertanyaan.index', compact('questions'));
    }
    public function show($pertanyaan_id){
        $question = DB::table('pertanyaan')->where('pertanyaan.id', $pertanyaan_id)->first();
        // dd($question);
        return view('pertanyaan.show', compact('question'));
        
    }
    public function edit($pertanyaan_id){
        $question = DB::table('pertanyaan')->where('pertanyaan.id', $pertanyaan_id)->first();
        // dd($question);
        return view('pertanyaan.edit', compact('question'));
    }
    public function update($pertanyaan_id, Request $request){
        $request->validate([
            'judul'=>'required|unique:pertanyaan|max:255',
            'isi'=>'required'
        ]);
        $question = DB::table('pertanyaan')
                    ->where('id', $pertanyaan_id)
                    ->update([
                        'judul'=>$request['judul'],
                        'isi'=>$request['isi'],
                    ]);
        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Diupdate');
    }

    public function destroy($pertanyaan_id){
        $question = DB::table('pertanyaan')
                    ->where('id',$pertanyaan_id)
                    ->delete();
        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Dihapus');
        
    }

}
